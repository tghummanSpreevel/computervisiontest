import pandas as pd
import string 
from nltk.tokenize import word_tokenize
import nltk
from nltk.corpus import stopwords
#nltk.download('punkt')
#nltk.download()

posSent=[]
negSent=[]

file=open("text.txt","r") 
f=file.readlines()

for line in f:
    if '1' in line:
        posSent.append(line[:-3])
    else:
        negSent.append(line[:-3])


dfPos = pd.DataFrame(posSent) 
dfNeg = pd.DataFrame(negSent)

dfPos.to_csv('posSen.txt', sep='\t', index=False)
dfNeg.to_csv('negSen.txt', sep='\t', index=False)


posText=open("posSen.txt").read()
negText=open("negSen.txt").read()

lowerCasePos=posText.lower() 
lowerCaseNeg=negText.lower() 


cleanedTextPos=lowerCasePos.translate(str.maketrans("","",string.punctuation))
cleanedTextNeg=lowerCaseNeg.translate(str.maketrans("","",string.punctuation))



tokenizedPosWords=cleanedTextPos.split()
tokenizedNegwords=cleanedTextNeg.split()

stop_words = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
             "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
             "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these",
             "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do",
              "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
              "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
              "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
              "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
              "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
              "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"]

finalPosWords=[]
finalNegWords=[]

for posWord in tokenizedPosWords:
    if posWord not in stop_words:
        finalPosWords.append(posWord)
    
for negWord in tokenizedNegwords:
    if negWord not in stop_words:
        finalNegWords.append(negWord)
    
#text_file = open('filename.txt', "w")
#n = text_file.write(finalPosWords)
#text_file.close()

dfPosf = pd.DataFrame(finalPosWords) 
dfNegf = pd.DataFrame(finalNegWords)

dfPosf.to_csv('posSenf1.csv', sep='\t', index=False)
dfNegf.to_csv('negSenf1.csv', sep='\t', index=False)
