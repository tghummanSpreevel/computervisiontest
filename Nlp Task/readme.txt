ON Pre-processing:

1. Text was divided into positive & negative sentiments
2. Then it is transformed into lower case and then cleaned with all punctuation marks.
3. Then it is okenized and the string is split for each word.
4. Then stop words are utilized to remove any further noise in our text.

Modelling:

1. After splitting the dataset into train & test. We have applied featurization in our cleaned-preprocessed text.
2. Featurization is done by simple TFIDF vectorizer which will convert these text to vectors.
3. We will be using Unigrams and Bigrams for TFIDF.
4. For training I have utilized the logistics regression, also utilized the RandomSeasrchCV to find the best tuning parameters.

How to run the code:

You just need to run the main file which is modelling.py.
To text the model you can enter one text having positive or negative sentiments in test_text.txt file.
Results will be shown on the command shell.

Future work:
I have just spend one full day on this task, I was in totonto and was not able to utilized whole 3 days on it.
However performance can be improved with utilzing better models such as LSTM in deep neural network. Also it might require more preprocessing to improve the noise more.

Feedback:
Task was really good, it really helped me to search about NLP more and to see how things are evolving around it.

Thanks. :)

