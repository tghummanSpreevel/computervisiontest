import pandas as pd
#import train_test_split
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
import string 

df=pd.read_csv("sentimental_analysis_data.csv")

#split the data into train and test datasets
reviews_train,reviews_test,sentiment_train,sentiment_test=train_test_split(df.X,df.Y)

#Applying TFIDF
tfidf_model=TfidfVectorizer(ngram_range=(1,2),min_df=10, max_features=6000)
tfidf_model.fit(reviews_train,sentiment_train)
reviews_train_tfidf=tfidf_model.transform(reviews_train)
reviews_test_tfidf=tfidf_model.transform(reviews_test)

from sklearn.linear_model import LogisticRegression 
from sklearn.metrics import f1_score
from sklearn.model_selection import RandomizedSearchCV
lr=LogisticRegression(max_iter=10000)

lr.fit(reviews_train_tfidf,sentiment_train)
lr_predict=lr.predict(reviews_test_tfidf)
plain_lr_f1=f1_score(sentiment_test,lr_predict,average=None)
print(plain_lr_f1)

lr_params={"penalty":["l1","l2"],
           "C":[10**i for i in range(-5,5)]}
lr=LogisticRegression()
lr_rndm_clf=RandomizedSearchCV(lr,lr_params)
lr_rndm_clf.fit(reviews_train_tfidf,sentiment_train)
lr_rndm_clf.best_params_

#passing the best pararms and creating a new model
lr_model=LogisticRegression(**lr_rndm_clf.best_params_,solver="liblinear")
lr_model.fit(reviews_train_tfidf,sentiment_train)
lr_predict=lr_model.predict(reviews_test_tfidf)
plain_lr_f1_best=f1_score(sentiment_test,lr_predict,average=None)
print("f-1 score on training set is as follows\n")
print(plain_lr_f1_best)

predict_text=[]

file=open("test_text.txt","r") 
f=file.readlines()

for line in f:
    predict_text.append(line)

dfPos = pd.DataFrame(predict_text) 
dfPos.to_csv('posSentext.txt', sep='\t', index=False)
posText=open("posSentext.txt").read()
lowerCasePos=posText.lower() 
cleanedTextPos=lowerCasePos.translate(str.maketrans("","",string.punctuation))
tokenizedPosWords=cleanedTextPos.split()

stop_words = ["i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
             "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself",
             "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these",
             "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do",
              "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
              "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
              "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
              "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
              "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
              "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"]

finalPosWords=[]

for posWord in tokenizedPosWords:
    if posWord not in stop_words:
        finalPosWords.append(posWord)

dfPosf = pd.DataFrame(finalPosWords) 
dfPosf=dfPosf.iloc[1::]


vect = tfidf_model.transform(dfPosf[0])
my_prediction = lr_model.predict(vect)


from scipy import stats
my_prediction_sentiments = stats.mode(my_prediction )
print("Sentimental Analysis for this particular text is as {} with 0:negative & 1: positive".format(my_prediction_sentiments[0]))
