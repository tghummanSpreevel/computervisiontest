import cv2
import math
import argparse
import time

from collections import OrderedDict
import numpy as np
import pandas as pd

def highlightFace(net, frame, conf_threshold=0.9):
    frameOpencvDnn=frame.copy()
    frameHeight=frameOpencvDnn.shape[0]
    frameWidth=frameOpencvDnn.shape[1]
    blob=cv2.dnn.blobFromImage(frameOpencvDnn, 1.0, (300, 300), [104, 117, 123], True, False)
    
    net.setInput(blob)
    detections=net.forward()
    faceBoxes=[]

    for i in range(detections.shape[2]):
        confidence=detections[0,0,i,2]
        if confidence>conf_threshold:
            x1=int(detections[0,0,i,3]*frameWidth)
            y1=int(detections[0,0,i,4]*frameHeight)
            x2=int(detections[0,0,i,5]*frameWidth)
            y2=int(detections[0,0,i,6]*frameHeight)
            faceBoxes.append([x1,y1,x2,y2])
            cv2.rectangle(frameOpencvDnn, (x1,y1), (x2,y2), (0,255,0), int(round(frameHeight/150)), 8)
           

    return frameOpencvDnn,faceBoxes

faceProto="opencv_face_detector.pbtxt"
faceModel="opencv_face_detector_uint8.pb"
genderProto="gender_deploy.prototxt"
genderModel="gender_net.caffemodel"

genderList=['Male','Female']
MODEL_MEAN_VALUES=(78.4263377603, 87.7689143744, 114.895847746)


faceNet=cv2.dnn.readNet(faceModel,faceProto)
genderNet=cv2.dnn.readNet(genderModel,genderProto)

video=cv2.VideoCapture("test.mp4")
padding=20

time_1=time.time()

df = pd.DataFrame([[0,0,0],[0,0,0],[0,0,0]], columns=list('XYG'))
df2 = pd.DataFrame([[0,0,0],[0,0,0],[0,0,0]], columns=list('XYG'))
df_3 = pd.DataFrame(columns=list('XYG'))
detect_gender = pd.DataFrame()
detect_gender_ = pd.DataFrame(columns=list('T'))

if (video.isOpened()==False):
    print("Error opening video stream")

while (video.isOpened()):

    hasFrame,frame=video.read()

    if hasFrame==True:
        resultImg,faceBoxes=highlightFace(faceNet,frame)

        if not faceBoxes:
            print("No face detected")

        for faceBox in faceBoxes:
            face=frame[max(0,faceBox[1]-padding):
                    min(faceBox[3]+padding,frame.shape[0]-1),max(0,faceBox[0]-padding)
                    :min(faceBox[2]+padding, frame.shape[1]-1)]

            blob=cv2.dnn.blobFromImage(face, 1.0, (227,227), MODEL_MEAN_VALUES, swapRB=False)
            genderNet.setInput(blob)
            genderPreds=genderNet.forward()
            gender=genderList[genderPreds[0].argmax()]
            print(f'Gender: {gender}')
            x1=faceBox[0]
            x2=faceBox[2]
            y1=faceBox[1]
            y2=faceBox[3]
            
            x_mean=float(x1+x2)/2
            y_mean=float(y1+y2)/2
            df2 = pd.DataFrame([[x_mean,y_mean,gender]], columns=list('XYG'))
            df=df.append(df2)
            time_2=time.time()
            J=df[['X', 'Y']].tail(2)
            
            if abs(J.X.iloc[-1]-J.X.iloc[-2])>5 and abs(J.Y.iloc[-1]-J.Y.iloc[-2])>5:
                df_3=df_3.append(df2)
            else:
                 continue  

            if (time_2-time_1)>5.0:
                count=df_3['G'].value_counts()
                count_name=df_3['G'].value_counts().idxmax()
                print(count)
                print("and")
                time_1=time.time()
                
                detect_gender=detect_gender.append(count,ignore_index=True)
                detect_gender_ = pd.DataFrame([[time_1]], columns=list('T'))
                detect_gender=detect_gender.append(detect_gender_,ignore_index=True,sort=True)
                detect_gender.to_csv('detections.csv',index=False)
                df_3=df_3.drop(columns=['X','Y','G'])
                df_3 =pd.DataFrame(columns=list('XYG'))

            cv2.putText(resultImg, f'{gender}', (faceBox[0], faceBox[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (254,255,255), 2, cv2.LINE_AA)
            cv2.imshow("Detecting gender", resultImg)

        if cv2.waitKey(2) & 0xFF==ord("q"):
            break
    
    else:
        break

video.release()
cv2.destroyAllWindows()